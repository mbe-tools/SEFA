SEFA (Switched Ethernet Flows Analysis) is an Eclipse plugin dedicated to the analysis of worst-case transmission time WCTT of packets on switched ethernet networks.

See https://mem4csd.telecom-paristech.fr/blog/index.php/switched-ethernet-flows-analysis/ for more infos.