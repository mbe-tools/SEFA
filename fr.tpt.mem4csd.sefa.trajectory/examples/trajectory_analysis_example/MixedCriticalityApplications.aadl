package MixedCriticalityApplications
public
	
	with MXC_Properties;
	
	process application
		features
			timing_failure: in event port;
			recovery: in event port;
			to_phase1: in event port;
			to_phase2: in event port;
	end application;
	
	process implementation application.mxc
		modes
			phase1: initial mode;
			phase2: mode;
			phase1 -[to_phase2]-> phase2;
			phase2 -[to_phase1]-> phase1;
	end application.mxc;
	
	thread group threads
		features
			-- TODO: connect timing_failure to HI or (LO to HI no vote) thread subcomponents? HI thread subcomponents?
			timing_failure: in event port;
			-- TODO: connect recovery to ? Processor? Hyperperiod? 
			-- Add in refined model to a scheduler thread at hyperperiod?
			-- TODO: mettre changement de mode à emergency pour LO-->HI et planned pour HI-->LO 
			recovery: in event port;
		modes
			LO: initial mode;
			HI: mode;
			LOtoHI: LO -[timing_failure]-> HI;
			HItoLO: HI -[recovery]-> LO;
		properties
			Mode_Transition_Response => planned applies to HItoLO;
			Mode_Transition_Response => emergency applies to LOtoHI;
	end threads;
	
	thread group application_1 extends threads
		features
			out_d: out data port d;
		flows
			fl_src: flow source out_d;
	end application_1;
	
	
	thread group implementation application_1.phase1
		subcomponents
			HI_out: thread thread1_HI_datadriven.impl in modes (HI,LO);
			HI_in: thread thread3_HI_datadriven.impl in modes (HI,LO);
			
			LO_out: thread thread1_LO_datadriven.impl in modes (LO);
			LO_in: thread thread2_LO_datadriven.impl in modes (LO);
			
		connections
			cnx: port HI_out.out_d -> HI_in.in_d;
			cnx2: port LO_out.out_d -> LO_in.in_d in modes (LO);
			cnx3: port LO_in.out_d -> out_d in modes (LO);
			
		flows
			fl_src: flow source LO_out.fl_src -> cnx2 -> LO_in.fl_path -> cnx3 -> out_d in modes (LO);
			fl_src: flow source out_d in modes (HI);
		properties

			Timing => immediate applies to cnx;
			Compute_Execution_Time => 5 ms .. 10 ms in modes(LO), 2 ms .. 12 ms in modes(HI) applies to HI_out;
			Compute_Execution_Time => 5 ms .. 10 ms in modes(LO), 2 ms .. 12 ms in modes(HI) applies to HI_in;
			Criticality => 4 applies to HI_out,HI_in;

			Timing => immediate applies to cnx2;
			Compute_Execution_Time => 5 ms .. 10 ms in modes(LO) applies to LO_out;
			Compute_Execution_Time => 5 ms .. 10 ms in modes(LO) applies to LO_in;
			Criticality => 0 applies to LO_out,LO_in;
			
			MXC_Properties::Overrun_Probability => 0.001 applies to HI_out, HI_in, LO_out, LO_in;
	end application_1.phase1;
	
	thread group implementation application_1.phase2
	end application_1.phase2;
	
	thread group application_2 extends threads
		features
			in_d: in data port d;
		flows
			fl_sink: flow sink in_d;
	end application_2;
	
	thread group implementation application_2.phase1
		subcomponents
			HI_out: thread thread1_HI_datadriven.impl in modes (HI,LO);
			HI_in: thread thread2_HI_datadriven.impl in modes (HI,LO);
			
			LO_out: thread thread2_LO_datadriven.impl in modes (LO);
			LO_in: thread thread3_LO_datadriven.impl in modes (LO);
			
		connections
			cnx: port HI_out.out_d -> HI_in.in_d;
			cnx2: port LO_out.out_d -> LO_in.in_d in modes (LO);
			cnx3: feature in_d -> LO_out.in_d in modes (LO);
		flows
			fl_sink: flow sink in_d -> cnx3 -> LO_out.fl_path -> cnx2 -> LO_in.fl_sink in modes (LO);
			fl_sink: flow sink in_d in modes (HI);
		properties
			Timing => immediate applies to cnx;
			Compute_Execution_Time => 5 ms .. 10 ms in modes(LO), 2 ms .. 12 ms in modes(HI) applies to HI_out;
			Compute_Execution_Time => 5 ms .. 10 ms in modes(LO), 2 ms .. 12 ms in modes(HI) applies to HI_in;
			Criticality => 4 applies to HI_out,HI_in;

			Timing => immediate applies to cnx2;
			Compute_Execution_Time => 5 ms .. 10 ms in modes(LO) applies to LO_out;
			Compute_Execution_Time => 5 ms .. 10 ms in modes(LO) applies to LO_in;
			Criticality => 0 applies to LO_out,LO_in;
			
			MXC_Properties::Overrun_Probability => 0.001 applies to HI_out, HI_in, LO_out, LO_in;
	end application_2.phase1;
	
	thread group implementation application_2.phase2
	end application_2.phase2;
	
	-- other options:
	--    	put criticality on subprograms
	--		put criticality on modes
	
	process application1 extends application
		features
			out_d: out data port d;
		flows
			fl_src: flow source out_d;
	end application1;
	
	process application2 extends application
		features
			in_d: in data port d;
		flows
			fl_sink: flow sink in_d;
	end application2;
	
	-- data driven simply means that precedence constraints
	-- are represented using data port connections
	process implementation application1.datadriven extends application.mxc
		subcomponents
			tg_phase1: thread group application_1.phase1;
			tg_phase2: thread group application_1.phase2;
		connections
			cnx: port tg_phase1.out_d -> out_d in modes (phase1);
			cnx2: port tg_phase2.out_d -> out_d in modes (phase2);
			cnx3: port timing_failure -> tg_phase1.timing_failure;
			cnx4: port recovery -> tg_phase1.recovery;
			cnx5: port timing_failure -> tg_phase2.timing_failure;
			cnx6: port recovery -> tg_phase2.recovery;
		flows
			fl_src: flow source tg_phase1.fl_src -> cnx -> out_d in modes (phase1);
			fl_src: flow source tg_phase2.fl_src -> cnx2 -> out_d in modes (phase2);
	end application1.datadriven;
	
	process implementation application2.datadriven extends application.mxc
		subcomponents
			tg_phase1: thread group application_2.phase1 in modes (phase1);
			tg_phase2: thread group application_2.phase2 in modes (phase2);
		connections
			cnx: port in_d -> tg_phase1.in_d in modes (phase1);
			cnx2: port in_d -> tg_phase2.in_d in modes (phase2);
			cnx3: port timing_failure -> tg_phase1.timing_failure;
			cnx4: port recovery -> tg_phase1.recovery;
			cnx5: port timing_failure -> tg_phase2.timing_failure;
			cnx6: port recovery -> tg_phase2.recovery;
		flows
			fl_sink: flow sink in_d -> cnx -> tg_phase1.fl_sink in modes (phase1);
			fl_sink: flow sink in_d  -> cnx2 -> tg_phase2.fl_sink in modes (phase2);
	end application2.datadriven;
	
	data d
	end d;
	
	thread simple_thread
	end simple_thread;
	
	thread LO_thread extends simple_thread
	requires modes
		LO: initial mode;
	end LO_thread;
	
	thread HI_thread extends simple_thread
	requires modes
		LO: initial mode;
		HI: mode;
	end HI_thread;
	
	thread implementation simple_thread.impl
		calls c: {
			c1: subprogram s1;
			c2: subprogram s2;
		};
		connections
			cnx1: parameter c1.param -> c2.param;
		properties
			Compute_Entrypoint_Call_Sequence => reference(c);
	end simple_thread.impl;
	
	thread thread1_HI_datadriven extends HI_thread
		features
			out_d: out data port d;
		flows
			fl_src: flow source out_d;
		properties
			Dispatch_Protocol => periodic;
	end thread1_HI_datadriven;
	
	thread implementation HI_thread.impl extends simple_thread.impl
	end HI_thread.impl;
	
	thread implementation thread1_HI_datadriven.impl extends HI_thread.impl
	end thread1_HI_datadriven.impl;
	
	thread thread2_HI_datadriven extends HI_thread
		features
			in_d: in data port d;
			out_d: out data port d;
		flows
			fl_path: flow path in_d -> out_d;
		properties
			Dispatch_Protocol => periodic;
	end thread2_HI_datadriven;
	
	thread implementation thread2_HI_datadriven.impl extends HI_thread.impl
	end thread2_HI_datadriven.impl;
	
	thread thread3_HI_datadriven extends HI_thread
		features
			in_d: in data port d;
		flows
			fl_sink : flow sink in_d;
		properties
			Dispatch_Protocol => periodic;
	end thread3_HI_datadriven;
	
	thread implementation thread3_HI_datadriven.impl extends HI_thread.impl
	end thread3_HI_datadriven.impl;
	
	thread implementation LO_thread.impl extends simple_thread.impl
	end LO_thread.impl;
		
	thread thread1_LO_datadriven extends LO_thread
		features
			out_d: out data port d;
		flows
			fl_src: flow source out_d;
		properties
			Dispatch_Protocol => periodic;
	end thread1_LO_datadriven;
	
	thread implementation thread1_LO_datadriven.impl extends LO_thread.impl
	end thread1_LO_datadriven.impl;
	
	thread thread2_LO_datadriven extends LO_thread
		features
			in_d: in data port d;
			out_d: out data port d;
		flows
			fl_path: flow path in_d -> out_d;
		properties
			Dispatch_Protocol => periodic;
	end thread2_LO_datadriven;
	
	thread implementation thread2_LO_datadriven.impl extends LO_thread.impl
	end thread2_LO_datadriven.impl;
	
	thread thread3_LO_datadriven extends LO_thread
		features
			in_d: in data port d;
		flows
			fl_sink : flow sink in_d;
		properties
			Dispatch_Protocol => periodic;
	end thread3_LO_datadriven;
	
	thread implementation thread3_LO_datadriven.impl extends LO_thread.impl
	end thread3_LO_datadriven.impl;
	
	
	subprogram mxc_subprogram
	requires modes
		   LO: initial mode;
		   HI: mode;
	end mxc_subprogram;
	
	subprogram s1 extends mxc_subprogram
		features
			param: out parameter d;
	end s1;
	
	subprogram s2 extends mxc_subprogram
		features
			param: in parameter d;	
	end s2;
	
	
end MixedCriticalityApplications;