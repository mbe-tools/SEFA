package fr.tpt.mem4csd.sefa.trajectory.exceptions;

import org.osate.aadl2.instance.ComponentInstance;


public class MalformedInputException extends Exception {

	public String message;
	public ComponentInstance obj;
	
	public MalformedInputException(String message, ComponentInstance obj) {
		this.message = message;
		this.obj=obj;
	};
	
}
