package fr.tpt.mem4csd.sefa.trajectory.exceptions;

public class NodeDoesNotHavePredecessor extends Exception {
	/**
   * 
   */
  private static final long serialVersionUID = 1L ;
  public String message;
	
	public NodeDoesNotHavePredecessor(String message) {
		this.message = message;
	}
}
