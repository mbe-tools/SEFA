package fr.tpt.mem4csd.sefa.trajectory.test;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import fr.tpt.mem4csd.sefa.trajectory.control.Algorithm;
import fr.tpt.mem4csd.sefa.trajectory.model.Flow;
import fr.tpt.mem4csd.sefa.trajectory.model.Network;
import fr.tpt.mem4csd.sefa.trajectory.model.Node;
import fr.tpt.mem4csd.sefa.trajectory.model.Path;

public class TrajectoryTest {

	public static void main(String[] arg) {
		Flow f1 = new Flow();
		f1.setPeriod(Long.valueOf(36));
		f1.setPriority(10);
		f1.setDeadline(36d);
		f1.setJitter(0);
		f1.setLmax(1.0d);
		f1.setLmin(1.0d);

		Flow f2 = new Flow();
		f2.setPeriod(Long.valueOf(36));
		f2.setPriority(10);
		f2.setDeadline(36d);
		f2.setJitter(0);
		f2.setLmax(1.0d);
		f2.setLmin(1.0d);
		
		Flow f3 = new Flow();
		f3.setPeriod(Long.valueOf(36));
		f3.setPriority(11);
		f3.setDeadline(54d);
		f3.setJitter(0);
		f3.setLmax(1.0d);
		f3.setLmin(1.0d);;

		
		Flow f4 = new Flow();
		f4.setPeriod(Long.valueOf(36));
		f4.setPriority(11);
		f4.setDeadline(54d);
		f4.setJitter(0);
		f4.setLmax(1.0d);
		f4.setLmin(1.0d);
		

		Flow f5 = new Flow();
		f5.setPeriod(Long.valueOf(36));
		f5.setPriority(12);
		f5.setDeadline(45d);
		f5.setJitter(0);
		f5.setLmax(1.0d);
		f5.setLmin(1.0d);

		Node n1 = new Node("n1");

		Map<Flow, Double> n1_capacity = new HashMap<Flow, Double>();
		n1_capacity.put(f1, 4.0d);
		n1_capacity.put(f2, 0.0d);
		n1_capacity.put(f3, 0.0d);
		n1_capacity.put(f4, 0.0d);
		n1_capacity.put(f5, 0.0d);
		n1.setCapacity(n1_capacity);

		Map<Integer, List<Flow>> inportN1 = new HashMap<Integer, List<Flow>>();
		List<Flow> flows_N1_1 = new ArrayList<Flow>();
		flows_N1_1.add(f1);
		inportN1.put(0, flows_N1_1);
		n1.setInputPort(inportN1);

		Node n2 = new Node("n2");
		Map<Flow, Double> n2_capacity = new HashMap<Flow, Double>();
		n2_capacity.put(f3, 4.0d);
		n2_capacity.put(f4, 4.0d);
		n2_capacity.put(f5, 4.0d);
		n2_capacity.put(f1, 0.0d);
		n2_capacity.put(f2, 0.0d);

		n2.setCapacity(n2_capacity);

		Map<Integer, List<Flow>> inportN2 = new HashMap<Integer, List<Flow>>();
		List<Flow> flows_N2_1 = new ArrayList<Flow>(), flows_N2_2 = new ArrayList<Flow>(), flows_N2_3 = new ArrayList<Flow>();
		flows_N2_1.add(f3);
		flows_N2_2.add(f4);
		flows_N2_3.add(f5);
		inportN2.put(0, flows_N2_1);
		inportN2.put(1, flows_N2_2);
		inportN2.put(2, flows_N2_3);
		n2.setInputPort(inportN2);

		Node n3 = new Node("n3");
		Map<Flow, Double> n3_capacity = new HashMap<Flow, Double>();
		n3_capacity.put(f1, 4.0d);
		n3_capacity.put(f3, 4.0d);
		n3_capacity.put(f4, 4.0d);
		n3_capacity.put(f5, 4.0d);
		n3_capacity.put(f2, 0.0d);
		n3.setCapacity(n3_capacity);

		Map<Integer, List<Flow>> inportN3 = new HashMap<Integer, List<Flow>>();

		List<Flow> flows_N3_1 = new ArrayList<Flow>(), flows_N3_2 = new ArrayList<Flow>(), flows_N3_3 = new ArrayList<Flow>(), flows_N3_4 = new ArrayList<Flow>();
		flows_N3_1.add(f1);
		flows_N3_2.add(f3);
		flows_N3_3.add(f4);
		flows_N3_4.add(f5);

		inportN3.put(0, flows_N3_1);
		inportN3.put(1, flows_N3_2);
		inportN3.put(2, flows_N3_3);
		inportN3.put(3, flows_N3_4);
		n3.setInputPort(inportN3);

		Node n4 = new Node("n4");
		Map<Flow, Double> n4_capacity = new HashMap<Flow, Double>();
		n4_capacity.put(f1, 4.0d);
		n4_capacity.put(f3, 4.0d);
		n4_capacity.put(f4, 4.0d);
		n4_capacity.put(f5, 4.0d);
		n4_capacity.put(f2, 0.0d);
		n4.setCapacity(n4_capacity);

		Map<Integer, List<Flow>> inportN4 = new HashMap<Integer, List<Flow>>();
		List<Flow> flows_N4_1 = new ArrayList<Flow>(), flows_N4_2 = new ArrayList<Flow>(), flows_N4_3 = new ArrayList<Flow>(), flows_N4_4 = new ArrayList<Flow>();
		flows_N4_1.add(f1);
		flows_N4_2.add(f3);
		flows_N4_3.add(f4);
		flows_N4_4.add(f5);
		inportN4.put(0, flows_N4_1);
		inportN4.put(1, flows_N4_2);
		inportN4.put(2, flows_N4_3);
		inportN4.put(3, flows_N4_4);
		n4.setInputPort(inportN4);

		Node n5 = new Node("n5");
		Map<Flow, Double> n5_capacity = new HashMap<Flow, Double>();
		n5_capacity.put(f1, 4.0d);
		n5_capacity.put(f2, 0.0d);
		n5_capacity.put(f3, 0.0d);
		n5_capacity.put(f4, 0.0d);
		n5_capacity.put(f5, 0.0d);
		n5.setCapacity(n5_capacity);

		Map<Integer, List<Flow>> inportN5 = new HashMap<Integer, List<Flow>>();
		List<Flow> flows_N5_1 = new ArrayList<Flow>();
		flows_N5_1.add(f1);
		inportN5.put(1, flows_N5_1);
		n5.setInputPort(inportN5);

		Node n6 = new Node("n6");
		Map<Flow, Double> n6_capacity = new HashMap<Flow, Double>();
		n6_capacity.put(f2, 4.0d);
		n6_capacity.put(f1, 0.0d);
		n6_capacity.put(f3, 0.0d);
		n6_capacity.put(f4, 0.0d);
		n6_capacity.put(f5, 0.0d);
		n6.setCapacity(n6_capacity);

		Map<Integer, List<Flow>> inportN6 = new HashMap<Integer, List<Flow>>();
		List<Flow> flows_N6_1 = new ArrayList<Flow>();
		flows_N6_1.add(f2);
		inportN6.put(0, flows_N6_1);
		n6.setInputPort(inportN6);

		Node n7 = new Node("n7");
		Map<Flow, Double> n7_capacity = new HashMap<Flow, Double>();
		n7_capacity.put(f2, 4.0d);
		n7_capacity.put(f3, 4.0d);
		n7_capacity.put(f4, 4.0d);
		n7_capacity.put(f5, 4.0d);
		n7_capacity.put(f1, 0.0d);
		n7.setCapacity(n7_capacity);

		Map<Integer, List<Flow>> inportN7 = new HashMap<Integer, List<Flow>>();
		List<Flow> flows_N7_1 = new ArrayList<Flow>(), flows_N7_2 = new ArrayList<Flow>(), flows_N7_3 = new ArrayList<Flow>(), flows_N7_4 = new ArrayList<Flow>();
		flows_N7_1.add(f2);
		flows_N7_2.add(f3);
		flows_N7_3.add(f4);
		flows_N7_4.add(f5);
		inportN7.put(0, flows_N7_1);
		inportN7.put(1, flows_N7_2);
		inportN7.put(2, flows_N7_3);
		inportN7.put(3, flows_N7_4);
		n7.setInputPort(inportN7);

		Node n8 = new Node("n8");
		Map<Flow, Double> n8_capacity = new HashMap<Flow, Double>();
		n8_capacity.put(f5, 4.0d);
		n8_capacity.put(f1, 0.0d);
		n8_capacity.put(f2, 0.0d);
		n8_capacity.put(f3, 0.0d);
		n8_capacity.put(f4, 0.0d);
		n8.setCapacity(n8_capacity);

		Map<Integer, List<Flow>> inportN8 = new HashMap<Integer, List<Flow>>();
		List<Flow> flows_N8_1 = new ArrayList<Flow>();
		flows_N8_1.add(f5);
		inportN8.put(0, flows_N8_1);
		n8.setInputPort(inportN8);

		Node n9 = new Node("n9");
		Map<Flow, Double> n9_capacity = new HashMap<Flow, Double>();
		n9_capacity.put(f2, 4.0d);
		n9_capacity.put(f1, 0.0d);
		n9_capacity.put(f3, 0.0d);
		n9_capacity.put(f4, 0.0d);
		n9_capacity.put(f5, 0.0d);
		n9.setCapacity(n9_capacity);

		Map<Integer, List<Flow>> inportN9 = new HashMap<Integer, List<Flow>>();
		List<Flow> flows_N9_1 = new ArrayList<Flow>();
		flows_N9_1.add(f2);
		inportN9.put(0, flows_N9_1);
		n9.setInputPort(inportN9);

		Node n10 = new Node("n10");
		Map<Flow, Double> n10_capacity = new HashMap<Flow, Double>();
		n10_capacity.put(f2, 4.0d);
		n10_capacity.put(f3, 4.0d);
		n10_capacity.put(f4, 4.0d);
		n10_capacity.put(f1, 0.0d);
		n10_capacity.put(f5, 0.0d);

		n10.setCapacity(n10_capacity);

		Map<Integer, List<Flow>> inportN10 = new HashMap<Integer, List<Flow>>();
		List<Flow> flows_N10_1 = new ArrayList<Flow>(), flows_N10_2 = new ArrayList<Flow>(), flows_N10_3 = new ArrayList<Flow>();
		flows_N10_1.add(f2);
		flows_N10_2.add(f3);
		flows_N10_3.add(f4);

		inportN10.put(0, flows_N10_1);
		inportN10.put(1, flows_N10_2);
		inportN10.put(2, flows_N10_3);
		n10.setInputPort(inportN10);

		Node n11 = new Node("n11");
		Map<Flow, Double> n11_capacity = new HashMap<Flow, Double>();
		n11_capacity.put(f3, 4.0d);
		n11_capacity.put(f4, 4.0d);
		n11_capacity.put(f1, 0.0d);
		n11_capacity.put(f2, 0.0d);
		n11_capacity.put(f5, 0.0d);

		n11.setCapacity(n11_capacity);

		Map<Integer, List<Flow>> inportN11 = new HashMap<Integer, List<Flow>>();
		List<Flow> flows_N11_1 = new ArrayList<Flow>(), flows_N11_2 = new ArrayList<Flow>();
		flows_N11_1.add(f3);
		flows_N11_2.add(f4);
		inportN11.put(0, flows_N11_1);
		inportN11.put(1, flows_N11_2);
		n11.setInputPort(inportN11);

		List<Node> nodes = new ArrayList<Node>();
		nodes.add(n1);
		nodes.add(n3);
		nodes.add(n4);
		nodes.add(n5);
		nodes.add(n9);
		nodes.add(n10);
		nodes.add(n7);
		nodes.add(n6);
		nodes.add(n2);
		nodes.add(n11);
		nodes.add(n8);

		List<Node> p1Nodes = new ArrayList<Node>();
		p1Nodes.add(n1);
		p1Nodes.add(n3);
		p1Nodes.add(n4);
		p1Nodes.add(n5);
		Path p1 = new Path(p1Nodes);
		f1.setPath(p1);

		List<Node> p2Nodes = new ArrayList<Node>();
		p2Nodes.add(n9);
		p2Nodes.add(n10);
		p2Nodes.add(n7);
		p2Nodes.add(n6);
		Path p2 = new Path(p2Nodes);
		f2.setPath(p2);

		List<Node> p3Nodes = new ArrayList<Node>();
		p3Nodes.add(n2);
		p3Nodes.add(n3);
		p3Nodes.add(n4);
		p3Nodes.add(n7);
		p3Nodes.add(n10);
		p3Nodes.add(n11);
		Path p3 = new Path(p3Nodes);
		f3.setPath(p3);

		List<Node> p4Nodes = new ArrayList<Node>();
		p4Nodes.add(n2);
		p4Nodes.add(n3);
		p4Nodes.add(n4);
		p4Nodes.add(n7);
		p4Nodes.add(n10);
		p4Nodes.add(n11);
		Path p4 = new Path(p4Nodes);
		f4.setPath(p4);

		List<Node> p5Nodes = new ArrayList<Node>();
		p5Nodes.add(n2);
		p5Nodes.add(n3);
		p5Nodes.add(n4);
		p5Nodes.add(n7);
		p5Nodes.add(n8);
		Path p5 = new Path(p5Nodes);
		f5.setPath(p5);

		List<Flow> flows = new ArrayList<Flow>();
		flows.add(f1);
		flows.add(f2);
		flows.add(f3);
		flows.add(f4);
		flows.add(f5);

		Network n = new Network(flows, nodes);
		n.init();

		System.out.println(n);
		
		// Simple example

		//
		// Flow f1bis = new Flow();
		// f1bis.setPeriod(20);
		// f1bis.setPriority(3);
		// f1bis.setDeadline(36);
		// f1bis.setJitter(0);
		//
		// Flow f2bis = new Flow();
		// f2bis.setPeriod(20);
		// f2bis.setPriority(2);
		// f2bis.setDeadline(36);
		// f2bis.setJitter(0);
		//
		// Flow f3bis = new Flow();
		// f3bis.setPeriod(20);
		// f3bis.setPriority(2);
		// f3bis.setDeadline(54);
		// f3bis.setJitter(0);
		//
		// Flow f4bis = new Flow();
		// f4bis.setPeriod(20);
		// f4bis.setPriority(1);
		// f4bis.setDeadline(54);
		// f4bis.setJitter(0);
		//
		//
		// Node n1bis = new Node();
		// n1bis.setId("n1bis");
		// Map<Flow, Integer> n1bis_capacity = new HashMap<Flow, Integer>();
		// n1bis_capacity.put(f1bis, 3);
		// n1bis_capacity.put(f2bis, 5);
		// n1bis_capacity.put(f3bis, 6);
		// n1bis_capacity.put(f4bis, 4);
		// n1bis.setCapacity((HashMap<Flow, Integer>) n1bis_capacity);
		//
		// HashMap<Integer, List<Flow>> inportN1bis = new HashMap<Integer,
		// List<Flow>>();
		//
		// List<Flow> flowsbis_N1_1 = new ArrayList<Flow>(), flowsbis_N1_2 = new
		// ArrayList<Flow>(), flowsbis_N1_3 = new ArrayList<Flow>(), flowsbis_N1_4 = new
		// ArrayList<Flow>();
		// flowsbis_N1_1.add(f1bis);
		// flowsbis_N1_2.add(f2bis);
		// flowsbis_N1_3.add(f3bis);
		// flowsbis_N1_4.add(f4bis);
		//
		// inportN1bis.put(1, flowsbis_N1_1);
		// inportN1bis.put(2, flowsbis_N1_2);
		// inportN1bis.put(3, flowsbis_N1_3);
		// inportN1bis.put(4, flowsbis_N1_4);
		// n1bis.setInputPort(inportN1bis);
		//
		//
		// Node n2bis = new Node();
		// n2bis.setId("n2bis");
		// Map<Flow, Integer> n2bis_capacity = new HashMap<Flow, Integer>();
		// n2bis_capacity.put(f1bis, 3);
		// n2bis_capacity.put(f2bis, 5);
		// n2bis_capacity.put(f3bis, 6);
		// n2bis_capacity.put(f4bis, 4);
		// n2bis.setCapacity((HashMap<Flow, Integer>) n2bis_capacity);
		//
		// HashMap<Integer, List<Flow>> inportN2bis = new HashMap<Integer,
		// List<Flow>>();
		//
		// List<Flow> flowsbis_N2_1 = new ArrayList<Flow>(), flowsbis_N2_2 = new
		// ArrayList<Flow>(), flowsbis_N2_3 = new ArrayList<Flow>(), flowsbis_N2_4 = new
		// ArrayList<Flow>();
		// flowsbis_N2_1.add(f1bis);
		// flowsbis_N2_2.add(f2bis);
		// flowsbis_N2_3.add(f3bis);
		// flowsbis_N2_4.add(f4bis);
		//
		// inportN2bis.put(1, flowsbis_N2_1);
		// inportN2bis.put(2, flowsbis_N2_2);
		// inportN2bis.put(3, flowsbis_N2_3);
		// inportN2bis.put(4, flowsbis_N2_4);
		// n2bis.setInputPort(inportN2bis);
		//
		//
		// Node n3bis = new Node();
		// n3bis.setId("n3bis");
		// Map<Flow, Integer> n3bis_capacity = new HashMap<Flow, Integer>();
		// n3bis_capacity.put(f1bis, 3);
		// n3bis_capacity.put(f2bis, 5);
		// n3bis_capacity.put(f3bis, 6);
		// n3bis_capacity.put(f4bis, 4);
		// n3bis.setCapacity((HashMap<Flow, Integer>) n3bis_capacity);
		//
		// HashMap<Integer, List<Flow>> inportN3bis = new HashMap<Integer,
		// List<Flow>>();
		//
		// List<Flow> flowsbis_N3_1 = new ArrayList<Flow>(), flowsbis_N3_2 = new
		// ArrayList<Flow>(), flowsbis_N3_3 = new ArrayList<Flow>(), flowsbis_N3_4 = new
		// ArrayList<Flow>();
		// flowsbis_N3_1.add(f1bis);
		// flowsbis_N3_2.add(f2bis);
		// flowsbis_N3_3.add(f3bis);
		// flowsbis_N3_4.add(f4bis);
		//
		// inportN3bis.put(1, flowsbis_N3_1);
		// inportN3bis.put(2, flowsbis_N3_2);
		// inportN3bis.put(3, flowsbis_N3_3);
		// inportN3bis.put(4, flowsbis_N3_4);
		// n3bis.setInputPort(inportN3bis);
		//
		//
		//
		// List<Node> nodesbis = new ArrayList<Node>();
		// nodesbis.add(n1bis);
		// nodesbis.add(n2bis);
		// nodesbis.add(n3bis);
		//
		//
		// List<Node> p1Nodesbis = new ArrayList<Node>();
		// p1Nodesbis.add(n1bis);
		// p1Nodesbis.add(n2bis);
		// p1Nodesbis.add(n3bis);
		//
		// Path p1bis = new Path(p1Nodesbis);
		// f1bis.setPath(p1bis);
		// f2bis.setPath(p1bis);
		// f3bis.setPath(p1bis);
		// f4bis.setPath(p1bis);
		//
		//
		//
		// List<Flow> flowsbis = new ArrayList<Flow>();
		// flowsbis.add(f1bis);
		// flowsbis.add(f2bis);
		// flowsbis.add(f3bis);
		// flowsbis.add(f4bis);
		//
		//
		// Network nbis = new Network(flowsbis, nodesbis, 2, 1);
		// nbis.init();

		Algorithm algo = new Algorithm(n);

		// Algorithm algo = new Algorithm(nbis);

		List<Double> results = algo.computeWorstCaseEndToEndResponse();
		System.out.println("Results: ");
		for (Double i : results)
			System.out.println(i);
		return;
	}
}
