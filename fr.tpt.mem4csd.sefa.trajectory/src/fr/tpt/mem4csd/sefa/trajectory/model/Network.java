package fr.tpt.mem4csd.sefa.trajectory.model;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class Network {
	List<Flow> flows;
	List<Node> nodes;
	
	public Network() {
		super();
		flows = new ArrayList<Flow>();
		nodes = new ArrayList<Node>();
	}
	
	public Network(List<Flow> flows, List<Node> nodes) {
		super();
		this.flows = flows;
		this.nodes = nodes;
	}
	
	@Override
	public String toString()
	{
		String res="Network description\n";
		int i=0;
		for(Flow f: this.flows)
		{
			i++;
			res+= "Flow "+ i + ", period " +f.getPeriod()+", Lmax "+f.getLmax();
			res+= ". Path = \n";
			for(Node n: f.getPath().getNodes())
			{
				res+="\t";
				res+=n.getId() + " - capacity ";
				assert(n != null); 
				assert(null != n.getCapacity()); 
				if(n.getCapacity()==null)n.setCapacity(new HashMap<Flow, Double>());
				Double cap = n.getCapacity().get(f);
				if(cap != null)
					res+=cap+"\n";
				else
					res+="0.0\n";
			}
			res+="\n";
		}
		
		for(Node n: nodes)
		{
			res+="Node " + n.getId() + "\n";
			res+="\t(Ports,Flow) = ";
			int j=0;
			if(n.getInputPort()!=null)
			for(List<Flow> flowList : n.getInputPort().values())
			{
				res+="\t\t(";
				if(flowList!=null)
				{
					res+=j+",";
					for(Flow fl:flowList)
					{
						res+=flows.indexOf(fl);
						res+= " ";
					}
				}
				res+=")\n";
				j++;
			}
			res+="\n";
		}
			
		return res;
	}
	
	/**
	 * Initialization of hpf, spf and lpf for each Flow
	 */
	public void init(){
		int i = 0, j = 0;
		Flow current_flow = null, compared_flow = null;
		for(i = 0; i < flows.size(); i++){
			List<Flow> hpf = new ArrayList<Flow>();
			List<Flow> spf = new ArrayList<Flow>();
			List<Flow> lpf = new ArrayList<Flow>();
			current_flow = flows.get(i); 
			for(j = 0; j < flows.size(); j++){
				compared_flow = flows.get(j);
				if(current_flow.getPriority() == compared_flow.getPriority()){
					if(j != i) spf.add(compared_flow);
				}
				else if(current_flow.getPriority() > compared_flow.getPriority()) lpf.add(compared_flow);
				else if(current_flow.getPriority() < compared_flow.getPriority()) hpf.add(compared_flow);
			}
			current_flow.setHigherPriorityFlows(hpf);
			current_flow.setLowerPriorityFlows(lpf);
			current_flow.setSamePriorityFlows(spf);
		}
	}
	
	public void addFlow(Flow flow) {
		flows.add(flow);
	}
	
	public void addNode(Node node) {
		nodes.add(node);
	}
	
	public List<Flow> getFlows() {
		return flows;
	}
	
	public void setFlows(List<Flow> flows) {
		this.flows = flows;
	}
	
	public List<Node> getNodes() {
		return nodes;
	}
	
	public void setNodes(List<Node> nodes) {
		this.nodes = nodes;
	}
	
	public Node findNodeById(String id){
		for(Node node : nodes){
			if(node.getId().equals(id)){
				return node;
			}
		}
		return null;
	}
	
	public Flow findFlowById(String id){
		for(Flow flow : flows){
			if(flow.getId().equals(id)){
				return flow;
			}
		}
		return null;
	}
	
	public void checkFlows() {
		for(Flow f : flows) {
			f.getPath().setNodeTypes();
		}
	}
	
	public void checkConnections() {
	}
}
