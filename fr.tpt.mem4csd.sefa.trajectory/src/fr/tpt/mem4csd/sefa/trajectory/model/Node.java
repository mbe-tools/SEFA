package fr.tpt.mem4csd.sefa.trajectory.model;

import java.util.List;
import java.util.Map;

public class Node {
	private Map<Flow, Double> capacity;
	private String id;
	private Map<Integer, List<Flow>> inPort;
	/** True for input ports, False for output ports **/
	private Type type;
	
	static public enum Type {
		Unset,
		EndSystem,
		Bus,
		Switch
	};
	

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public Node(String id, Type type) {
		this.id = id;
		this.type = type;
	}

	public Node(String id) {
		this.id = id;
	}

	public Map<Flow, Double> getCapacity() {
		return capacity;
	}

	public void setCapacity(Map<Flow, Double> capacity) {
		this.capacity = capacity;
	}

	public Map<Integer, List<Flow>> getInputPort() {
		return inPort;
	}

	public void setInputPort(Map<Integer, List<Flow>> inport) {
		this.inPort = inport;
	}

	public Type getType() {
		return type;
	}

	public void setType(Type type) {
		assert(type == this.type || this.type==Type.Unset);
		this.type = type;
	}
}
