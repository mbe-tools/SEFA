package fr.tpt.mem4csd.sefa.trajectory.model;

import java.util.ArrayList;
import java.util.List;

//import fr.tpt.mem4csd.sefa.trajectory.exceptions.NodeDoesNotExistException;

public class Path {
	private List<Node> nodes;

	public Path(List<Node> nodes) {
		this.nodes = nodes;
	}

	public Path() {
		this.nodes = new ArrayList<Node>();
	}

	public void setNode(Node node) {
		nodes.add(node);
	}

	public List<Node> getNodes() {
		return nodes;
	}

	public void setNodes(List<Node> nodes) {
		this.nodes = nodes;
	}
	
	public void setNodeTypes() {
		assert(nodes.size()%2==1);
		for(int i=0;i<nodes.size();++i) {
			if(i==0 || i+1==nodes.size()) {nodes.get(i).setType(Node.Type.EndSystem); }
			else if(i%2 == 1) {nodes.get(i).setType(Node.Type.Bus);}
			else {nodes.get(i).setType(Node.Type.Switch);}
		}
	}
	
	public void setNodeDirection() {}//TODO

	/***********************/
	/** Path up to h (included) **/
	public Path pathRestrictedToH(Node h) {
		Path p = new Path();
		if (this.getNodes().contains(h)) {
			p.setNodes(this.nodes.subList(0, nodes.indexOf(h) + 1));
		}
		return p;
	}
	/**************************/
}
