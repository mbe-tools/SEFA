package fr.tpt.mem4csd.sefa.trajectory.model;

import java.util.ArrayList;
import java.util.List;

//public class Flow extends CommunicationElement {
public class Flow {
	private Path path;
	private long priority;
	private Double deadline;
	private long period;
	private Double lmax;
	private Double lmin;
	
	private double jitter;
	private List<Flow> higherPriorityFlows;
	private List<Flow> samePriorityFlows;
	private List<Flow> LowerPriorityFlows;
	private String id;
	private double wcrt;
	private int size;
	private boolean isSubFlow;


	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public Flow() {
		super();
		higherPriorityFlows = new ArrayList<Flow>();
		samePriorityFlows = new ArrayList<Flow>();
		LowerPriorityFlows = new ArrayList<Flow>();
	}

	public Flow(String id) {
		super();
		setId(id);
	}
	
	public Flow(	Path path, 
					long priority, 
					double deadline,
					long periode,
					double jitter,
					String id,
					int size,
					boolean isSubFlow ) {
		super();
		
		this.path = path;
		this.priority = priority;
		this.deadline = deadline;
		this.period = periode;
		this.jitter = jitter;
		this.id = id;
		this.size = size;
		this.isSubFlow = isSubFlow;
	}

	public Path getPath() {
		return path;
	}

	public void setPath(Path path) {
		this.path = path;
	}

	public long getPriority() {
		return priority;
	}

	public void setPriority(long priority) {
		this.priority = priority;
	}

	public Double getDeadline() {
		return deadline;
	}

	public void setDeadline(Double deadline) {
		this.deadline = deadline;
	}

	public long getPeriod() {
		return period;
	}

	public void setPeriod(long periode) {
		this.period = periode;
	}

	public Double getLmax() {
		return lmax;
	}
	
	public void setLmax(Double lmax) {
		this.lmax = lmax;
	}
	
	public Double getLmin() {
		return lmin;
	}
	
	public void setLmin(Double lmin) {
		this.lmin = lmin;
	}
	
	public double getJitter() {
		return jitter;
	}

	public void setJitter(double jitter) {
		this.jitter = jitter;
	}

	public List<Flow> getHigherPriorityFlows() {
		return higherPriorityFlows;
	}

	public void setHigherPriorityFlows(List<Flow> higherPriorityFlows) {
		this.higherPriorityFlows = higherPriorityFlows;
	}

	public List<Flow> getSamePriorityFlows() {
		return samePriorityFlows;
	}

	public void setSamePriorityFlows(List<Flow> samePriorityFlows) {
		this.samePriorityFlows = samePriorityFlows;
	}

	public List<Flow> getLowerPriorityFlows() {
		return LowerPriorityFlows;
	}

	public void setLowerPriorityFlows(List<Flow> lowerPriorityFlows) {
		LowerPriorityFlows = lowerPriorityFlows;
	}

	public double getWcrt() {
		return wcrt;
	}

	public void setWcrt(double wcrt) {
		this.wcrt = wcrt;
	}

	public int getSize() {
		return size;
	}

	public void setSize(int size) {
		this.size = size;
	}

	public boolean isSubFlow() {
		return isSubFlow;
	}

	public void setSubFlow(boolean isSubFlow) {
		this.isSubFlow = isSubFlow;
	}
}
