package fr.tpt.mem4csd.sefa.trajectory.control;

import java.util.List;

import fr.tpt.mem4csd.sefa.trajectory.model.Network;

public class TrajectoryAnalysisAlgorithm {

	public static void responseTimeAnalysis( List<Network> networkList) {
		for (Network net : networkList) {
			Algorithm algo = new Algorithm(net);
			algo.computeWorstCaseEndToEndResponse();
		}
	}
}
