package fr.tpt.mem4csd.sefa.trajectory.control;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IAdaptable;
import org.eclipse.core.runtime.NullProgressMonitor;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl;
import org.eclipse.emf.edit.domain.AdapterFactoryEditingDomain;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.ui.handlers.HandlerUtil;
import org.osate.aadl2.Element;
import org.osate.aadl2.instance.ComponentInstance;
import org.osate.aadl2.instance.SystemInstance;
import org.osate.aadl2.modelsupport.AadlConstants;
import org.osate.aadl2.modelsupport.errorreporting.AnalysisErrorReporterManager;
import org.osate.aadl2.modelsupport.errorreporting.MarkerAnalysisErrorReporter;
import org.osate.aadl2.modelsupport.resources.OsateResourceUtil;
import org.osate.aadl2.parsesupport.LocationReference;
import org.osate.utils.internal.Aadl2Utils;

import fr.tpt.mem4csd.analysis.model.analysis.AnalysisArtifact;
import fr.tpt.mem4csd.analysis.model.analysis.AnalysisResultFactory;
import fr.tpt.mem4csd.sefa.trajectory.exceptions.MalformedInputException;
import fr.tpt.mem4csd.sefa.trajectory.model.Flow;
import fr.tpt.mem4csd.sefa.trajectory.model.Node;

public class AadlToTrajectoryActionHandler extends AbstractHandler {
	
	private final AnalysisErrorReporterManager errorReporterManager;
	
	public AadlToTrajectoryActionHandler() {
		errorReporterManager = new AnalysisErrorReporterManager( new MarkerAnalysisErrorReporter.Factory( AadlConstants.INSTANTIATION_OBJECT_MARKER ) );
	}

	private static SystemInstance getSelectedSystem(final ISelection p_selection) {
		SystemInstance newObject = null;

		if ( p_selection instanceof IStructuredSelection ) {
			final IStructuredSelection structSel =
					(IStructuredSelection) p_selection;

			if ( structSel.size() == 1 ) {
				final Object element =
						AdapterFactoryEditingDomain.unwrap( ( (IStructuredSelection) p_selection
								).getFirstElement() );

				if ( element instanceof SystemInstance ) {
					newObject = (SystemInstance) element;
				}
				else if ( element instanceof IResource) {
					IResource ires = (IResource) element;
					if("aaxl2".equals(ires.getFileExtension())) {
						URI uri = OsateResourceUtil.toResourceURI((IResource)element);
						ResourceSet rs = new ResourceSetImpl();
						Resource resource =  rs.getResource(uri, true);
						newObject = (SystemInstance) resource.getContents().get(0);
					}
				}
				else if ( element instanceof IAdaptable ) {
					newObject = (SystemInstance) ( (IAdaptable) element
							).getAdapter( SystemInstance.class );
				}
			}
		}

		return newObject;
	}

	@Override
	public Object execute(ExecutionEvent event) throws ExecutionException {

		ISelection selection = HandlerUtil.getCurrentSelection(event);
		SystemInstance instance = getSelectedSystem(selection);


		final LatencyAnalyzer la = new LatencyAnalyzer();

		try {
			//TODO use performAnalysis
			TrajectoryAnalysisResult ar = la.trajectoryAnalysis(instance);

			AnalysisResultFactory f = AnalysisResultFactory.eINSTANCE ;
			AnalysisArtifact result = f.createAnalysisArtifact() ;
			ar.normalize(result);


			Resource resource = instance.eResource();
			URI resUri = resource.getURI().trimFileExtension().appendFileExtension( "analysis" );
			ResourceSet rset = resource.getResourceSet();
			Resource analysisResource = rset.getResource(resUri, false);
			if(analysisResource==null)
				analysisResource = rset.createResource(resUri);
			analysisResource.getContents().add(result);
			
			try {
				analysisResource.save(null);
			} catch (IOException e) {
				throw new ExecutionException(e.getMessage(),e);
			}

			String path="";
			URI dirURI = resource.getURI();
			String projectName = dirURI.toPlatformString(true).substring(1);
			projectName = projectName.substring(0,projectName.indexOf("/"));
			IProject project = ResourcesPlugin.getWorkspace().getRoot().getProject(projectName);

			path = project.getLocation().toOSString();

			String filePathWithProject = dirURI.toPlatformString(true);
			filePathWithProject = filePathWithProject.substring(filePathWithProject.indexOf("/")+projectName.length()+1);

			path = path + filePathWithProject ;
			int index = path.lastIndexOf(".");
			path = path.substring(0, index);
			File f1 = new File(path.concat("-sefa.csv"));
			try {
				PrintWriter writer = new PrintWriter(f1);
				writer.println("");

				for(ComponentInstance ci: ar._virtualLink2WCTT.keySet()) {
					DetailLogger res = ar._virtualLink2WCTT.get(ci);
					Flow flow = ar._fm.get(ci);
					for(DetailLogger.Log l:res.cur.get(0).a) {
						if(l.n==null && l.value != 0)
							writer.println((l.f==null ? "" : l.f.getId())+" , "+","+l.value +", "+ (l.reason==null ? "":l.reason));
					}
					for(Node nn:flow.getPath().getNodes()) {
						for(DetailLogger.Log l:res.cur.get(0).a) {
							if(l.n==nn) writer.println((l.f==null ? "" : l.f.getId()) +", "+l.n.getId()+", "+l.value + ", " + (l.reason==null ? "":l.reason));
						}
					}
					writer.println("WCTT for flow, "+ci.getFullName()+", "+res.cur.get(0).value+ ", WCTT means Worst Case Transmission Time\n");
				}
				
				writer.close();
			
				project.refreshLocal( IResource.DEPTH_INFINITE, new NullProgressMonitor() );
			}
			catch ( final FileNotFoundException ex ) {
				ex.printStackTrace();
				
				throw new ExecutionException( ex.getMessage(), ex );
			}
			catch ( final CoreException ex ) {
				ex.printStackTrace();
				
				throw new ExecutionException( ex.getMessage(), ex );
			}
//			try {
//				project.refreshLocal(IResource.DEPTH_INFINITE, new NullProgressMonitor());
//			} catch (CoreException e) {
//				e.printStackTrace();
//			}
		}
		catch ( final MalformedInputException ex ) {
			// TODO: should be done in OSATE error manager
			Element elt = (Element) ex.obj;
			LocationReference lr = Aadl2Utils.getLocationReference( elt );
			elt.setLocationReference(lr);
			errorReporterManager.error( ex.obj, ex.message );
//			try {
//				final IResource resource = OsateResourceUtil.convertToIResource(instance.eResource());
//				final IMarker marker = resource.createMarker( EValidator.MARKER );
//				marker.setAttribute( IMarker.MESSAGE, e2.message );
//				marker.setAttribute( IMarker.SEVERITY, IMarker.SEVERITY_ERROR );
//				marker.setAttribute( EValidator.URI_ATTRIBUTE,
//						EcoreUtil.getURI(e2.obj).toString());
//			} catch (Exception ee) {
//				//Failed to fail
//				throw new ExecutionException(ee.getMessage(),ee);
//			}
//			e2.printStackTrace();
//			throw new ExecutionException(e2.getMessage(),e2);
		}
		
		System.out.println("done");
		
		return null;
	}
}
