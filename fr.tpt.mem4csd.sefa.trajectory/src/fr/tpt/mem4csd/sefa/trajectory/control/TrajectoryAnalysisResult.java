package fr.tpt.mem4csd.sefa.trajectory.control;

import java.util.Map;

import org.osate.aadl2.instance.ComponentInstance;

import fr.tpt.mem4csd.analysis.model.analysis.AnalysisArtifact;
import fr.tpt.mem4csd.analysis.model.analysis.AnalysisResultFactory;
import fr.tpt.mem4csd.analysis.model.analysis.AnalysisSource;
import fr.tpt.mem4csd.analysis.model.analysis.QualitativeAnalysisResult;
import fr.tpt.mem4csd.analysis.model.analysis.QuantitativeAnalysisResult;
import fr.tpt.mem4csd.sefa.trajectory.model.Flow;

public class TrajectoryAnalysisResult {

	Map<ComponentInstance, DetailLogger> _virtualLink2WCTT;
	Map<ComponentInstance, Double> _deadlines;
	Map<ComponentInstance, Flow> _fm;
	
	public TrajectoryAnalysisResult(Map<ComponentInstance, DetailLogger> results,
			Map<ComponentInstance, Double> deadlines, Map<ComponentInstance, Flow> flowMap)
	{
		_virtualLink2WCTT = results;
		_deadlines = deadlines;
		_fm = flowMap;
	}
	
	public void normalize(AnalysisArtifact result) {
		AnalysisResultFactory factory = AnalysisResultFactory.eINSTANCE;
		
		
		String nfpId = "Worst Case Transmission Time of Packets on Ethernet Switched Networks";
		String methodName = "Trajectory approach on ethernet switched networks";
		
		for(ComponentInstance ci : _virtualLink2WCTT.keySet())
		{
			QuantitativeAnalysisResult quantar = factory.createQuantitativeAnalysisResult();
			quantar.setNfpId(nfpId);
			
			AnalysisSource as = factory.createAnalysisSource();
			as.setMethodName(methodName);
			as.setScope(ci.getComponentInstancePath());
			
			quantar.setSource(as);
			
			quantar.setValue((float)_virtualLink2WCTT.get(ci).cur.get(0).value);
			
			if(_deadlines.get(ci)!=null && _deadlines.get(ci)>0)
			{
				quantar.setMargin((float) ((_deadlines.get(ci)-(float)_virtualLink2WCTT.get(ci).cur.get(0).value)/_deadlines.get(ci)));
				
				QualitativeAnalysisResult qualar = factory.createQualitativeAnalysisResult();
			
				AnalysisSource as_qual = factory.createAnalysisSource();
				as_qual.setMethodName(methodName);
				as_qual.setScope(ci.getComponentInstancePath());
				qualar.setSource(as_qual);
				qualar.setNfpId(nfpId);
				boolean res = _virtualLink2WCTT.get(ci).cur.get(0).value<_deadlines.get(ci);
				qualar.setValidated(res);
				result.getResults().add(qualar);
			}
			
			result.getResults().add(quantar);
		}
	}
}
