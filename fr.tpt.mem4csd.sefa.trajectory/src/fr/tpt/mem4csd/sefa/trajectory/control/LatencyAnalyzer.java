package fr.tpt.mem4csd.sefa.trajectory.control;

import java.util.ArrayList ;
import java.util.HashMap ;
import java.util.List ;
import java.util.Map ;

import org.apache.log4j.Logger ;
import org.eclipse.core.runtime.IProgressMonitor ;
import org.eclipse.xtext.EcoreUtil2;
import org.osate.aadl2.AadlInteger;
import org.osate.aadl2.ComponentCategory ;
import org.osate.aadl2.IntegerLiteral;
import org.osate.aadl2.ModalPropertyValue;
import org.osate.aadl2.Property;
import org.osate.aadl2.PropertyAssociation;
import org.osate.aadl2.PropertyExpression;
import org.osate.aadl2.RealLiteral;
import org.osate.aadl2.RefinableElement;
import org.osate.aadl2.UnitLiteral;
import org.osate.aadl2.instance.ComponentInstance;
import org.osate.aadl2.instance.ConnectionInstanceEnd;
import org.osate.aadl2.instance.FeatureCategory;
import org.osate.aadl2.instance.FeatureInstance ;
import org.osate.aadl2.instance.SystemInstance ;
import org.osate.aadl2.modelsupport.errorreporting.AnalysisErrorReporterManager ;
import org.osate.utils.internal.PropertyUtils;
import org.osate.xtext.aadl2.properties.util.AadlProject;
import org.osate.xtext.aadl2.properties.util.GetProperties;

import fr.tpt.mem4csd.sefa.trajectory.exceptions.MalformedInputException;
import fr.tpt.mem4csd.sefa.trajectory.model.Flow;
import fr.tpt.mem4csd.sefa.trajectory.model.Network;
import fr.tpt.mem4csd.sefa.trajectory.model.Node;
import fr.tpt.mem4csd.sefa.trajectory.model.Path;

public class LatencyAnalyzer //extends AbstractAnalyzer
{

	private final static Logger _LOGGER = Logger.getLogger(LatencyAnalyzer.class) ;

	//private static final String REGISTRY_NAME = "SEFA-SwitchedEthernetFlowAnalysis" ;

	//	AnalysisArtifact currentResult;
	
	public LatencyAnalyzer()	{
	}

	//@Override
//	public String getRegistryName()
//	{
//		return REGISTRY_NAME ;
//	}

	private List<ComponentInstance> getComponentInstances(ComponentInstance ci, 
			ComponentCategory category)
	{
		List<ComponentInstance> res = new ArrayList<ComponentInstance>();
		for(ComponentInstance i: EcoreUtil2.getAllContentsOfType(ci, ComponentInstance.class))
		{
			if(i.getCategory().equals(category))
				res.add(i);
		}
		return res;
	}

	public void performAnalysis(SystemInstance systemInstance,
			AnalysisErrorReporterManager errManager,
			IProgressMonitor monitor,SystemInstance firstSysInstance)
	throws MalformedInputException {
		trajectoryAnalysis(systemInstance);
	}

	private Long checkLongAttribute(ComponentInstance virtualBus, String attr)
	throws MalformedInputException {
		PropertyAssociation pa = PropertyUtils.findPropertyAssociation(attr, virtualBus);

	    if (pa != null) {
	      Property p = pa.getProperty();

	      if (p.getName().equalsIgnoreCase(attr)) {
	        List<ModalPropertyValue> values = pa.getOwnedValues();

	        if (values.size() == 1) {
	          ModalPropertyValue v = values.get(0);
	          PropertyExpression expr = v.getOwnedValue();

	          if (expr instanceof IntegerLiteral) {
	            IntegerLiteral il = (IntegerLiteral) expr;
	            final UnitLiteral unit = il.getUnit();
	            String targetUnit=null;
	            if (unit != null) {
	              if (pa.getProperty().getPropertyType() instanceof AadlInteger) {
	                AadlInteger ai = (AadlInteger) pa.getProperty().getPropertyType();
	                if (ai.getUnitsType().getName().equalsIgnoreCase(AadlProject.SIZE_UNITS))
	                     targetUnit = AadlProject.B_LITERAL;
	                if (ai.getUnitsType().getName().equalsIgnoreCase(AadlProject.TIME_UNITS))
	                	targetUnit = AadlProject.MS_LITERAL;
	              }
	              // Warning: the cast from double to long is licit
	              // only if the result of the conversion is an
	              // integer
	              if(targetUnit!=null)
	            	  return (long) il.getScaledValue(targetUnit);
	            }
	            return il.getValue();
	          }
	        }
	      }
	    }
		
		String errMsg = "cannot fetch "+attr+" property for \'" +
				virtualBus.getComponentInstancePath() + '\'' ;
		_LOGGER.error(errMsg) ;
		throw new MalformedInputException( errMsg, virtualBus );
		
	}

	private Double checkDoubleAttribute(ComponentInstance virtualBus, String attr)
	throws MalformedInputException {
		Double value = null;
		PropertyAssociation pa = PropertyUtils.findPropertyAssociation(attr, virtualBus);

	    if (pa != null) {
	      Property p = pa.getProperty();

	      if (p.getName().equalsIgnoreCase(attr)) {
	        List<ModalPropertyValue> values = pa.getOwnedValues();

	        if (values.size() == 1) {
	          ModalPropertyValue v = values.get(0);
	          PropertyExpression expr = v.getOwnedValue();

	          if (expr instanceof IntegerLiteral) {
	        	  IntegerLiteral il = (IntegerLiteral) expr;
	        	  final UnitLiteral unit = il.getUnit();
	        	  String targetUnit=null;
	        	  if (unit != null) {
	        		  if (pa.getProperty().getPropertyType() instanceof AadlInteger) {
	        			  AadlInteger ai = (AadlInteger) pa.getProperty().getPropertyType();
	        			  if (ai.getUnitsType().getName().equalsIgnoreCase(AadlProject.SIZE_UNITS))
	        				  targetUnit = AadlProject.B_LITERAL;
	        			  if (ai.getUnitsType().getName().equalsIgnoreCase(AadlProject.TIME_UNITS))
	        				  targetUnit = AadlProject.MS_LITERAL;
	        		  }
	        		  if(targetUnit!=null)
	        			  value = il.getScaledValue(targetUnit);
	        		  else
	        			  value = il.getScaledValue();
	        	  }

	          } else if (expr instanceof RealLiteral) {
	            value =  (double) ((RealLiteral) expr).getValue();
	          }
	        }
	      }
	    }
	    
		if(value == null)
		{
			String errMsg = "cannot fetch "+attr+" property for \'" +
					virtualBus.getComponentInstancePath() + '\'' ;
			_LOGGER.error(errMsg) ;
//			ServiceProvider.SYS_ERR_REP.error(errMsg, true) ;
			throw new MalformedInputException( errMsg, virtualBus );
		}

		return value;
	}

	public TrajectoryAnalysisResult trajectoryAnalysis(SystemInstance systemInstance)
	throws MalformedInputException {
		List<Flow> flows = new ArrayList<Flow>();
		List<Node> nodes = new ArrayList<Node>();
		Map<ComponentInstance, Node> nodesMap = 
				new HashMap<ComponentInstance, Node>();

		Map<ComponentInstance, Flow> flowMap =
				new HashMap<ComponentInstance, Flow>();

		Map<Flow, ComponentInstance> flow2CI =
				new HashMap<Flow, ComponentInstance>();

		// get virtual buses (flows)
		List<ComponentInstance> virtualBusList = 
				getComponentInstances(systemInstance, ComponentCategory.VIRTUAL_BUS);

		for(ComponentInstance virtualBus:virtualBusList)
		{ // for each flow, populate its attributes.
			Flow f = new Flow(virtualBus.getComponentInstancePath());
			//fetch&check properties
			try {
				Long period = checkLongAttribute(virtualBus, "BAG"); 
				f.setPeriod(period);
				Long priority = checkLongAttribute(virtualBus, "priority"); 
				f.setPriority(priority);
				Long Lmax = checkLongAttribute(virtualBus, "Lmax"); // Bytes
				Long portSpeed = checkLongAttribute(systemInstance, "portSpeed"); // MBytes per sec
				f.setLmax(Double.valueOf(Lmax)/Float.valueOf(portSpeed*1000));// conversion in Bytes per s
				Double Lmin = checkDoubleAttribute(virtualBus, "Lmin");
				f.setLmin(Double.valueOf(Lmin)/Double.valueOf(portSpeed*1000));
				Double jitter = checkDoubleAttribute(virtualBus, "jitter");
				f.setJitter(jitter);
				Double deadline = checkDoubleAttribute(virtualBus, "Deadline");
				f.setDeadline(deadline);
			}
			catch(MalformedInputException e) {
				String errMsg = "Could not fetch a required attribute:"+e.message;
				_LOGGER.error(errMsg);
				
				throw new MalformedInputException(errMsg, virtualBus);
			}

			flowMap.put(virtualBus, f);
			flow2CI.put(f,virtualBus);
			flows.add(f);
		}

		for(ComponentInstance virtualBus:virtualBusList)
		{   // for each flow, 
			List<ComponentInstance> connectionBindingList = 
					PropertyUtils.getComponentInstanceList(virtualBus,
							"Actual_Connection_Binding");

			if(connectionBindingList==null || connectionBindingList.isEmpty()) {
				String errMsg = "flow"+virtualBus.getFullName()+"is empty !";
				_LOGGER.error(errMsg);
				
				throw new MalformedInputException(errMsg, virtualBus);
			}
			
			List<Node> listNodes = new ArrayList<Node>();

			// check that odd-numbered are buses and even-numbered are end systems.
			int l = connectionBindingList.size();

			if(l%2 != 1) {
				String errMsg = "flow "+virtualBus.getFullName()+" has an even number of components.";
				_LOGGER.error(errMsg);
				
				throw new MalformedInputException(errMsg, virtualBus);
			}
			
			for(int i=0;i<l;i+=2) {
				if(connectionBindingList.get(i).getCategory()==ComponentCategory.BUS) {
					String errMsg = "flow"+virtualBus.getFullName()+" has a bus in even position"+i;
					_LOGGER.error(errMsg);
					
					throw new MalformedInputException(errMsg, virtualBus);
				}
			}
			for(int i=1;i<l;i+=2) {
				if(connectionBindingList.get(i).getCategory()!=ComponentCategory.BUS) {
					String errMsg = "flow"+virtualBus.getFullName()+" has an device in odd position"+i;
					_LOGGER.error(errMsg);
					
					throw new MalformedInputException(errMsg, virtualBus);
				}
			}
			
			for(ComponentInstance nwkSwitch:connectionBindingList)
			{
				//bus
				if (nwkSwitch.getCategory()==ComponentCategory.BUS)
					continue;

				//populate nodes (switches/end systems)
				Node n;
				if(! nodesMap.containsKey(nwkSwitch))
				{
					n = new Node(nwkSwitch.getComponentInstancePath());
					nodes.add(n);
					nodesMap.put(nwkSwitch, n);


					boolean isNode = 
							nwkSwitch.getCategory().equals(ComponentCategory.DEVICE) ||
							nwkSwitch.getCategory().equals(ComponentCategory.PROCESSOR) ||
							nwkSwitch.getCategory().equals(ComponentCategory.SYSTEM);
					if(!isNode) {
						String errMsg = "Given its potision in the flow, object "+nwkSwitch.getFullName()+" should be a device, a processor, or a system.";
						_LOGGER.error(errMsg);
						throw new MalformedInputException(errMsg, nwkSwitch);
					}

					if(isNode) {
						Double cap;					
						try {cap = checkDoubleAttribute(nwkSwitch,"maxDelay");} 
						catch(Exception e) { cap = (double) 0; }

						Map<Flow, Double> capacity = new HashMap<Flow, Double>();
						for(ComponentInstance vBus:virtualBusList) {
							List<ComponentInstance> cbl = 
									PropertyUtils.getComponentInstanceList(virtualBus,
											"Actual_Connection_Binding");
							if(cbl.contains(nwkSwitch)) {
								capacity.put(flowMap.get(vBus),cap);
							}
						}
						n.setCapacity(capacity);
					} 
				} else { n = nodesMap.get(nwkSwitch); } 
				listNodes.add(n);
			}

			flowMap.get(virtualBus).setPath(new Path(listNodes));
		}

		Map<FeatureInstance, List<Flow>> featureInstance2inFlowList = new HashMap<FeatureInstance, List<Flow>>();
		
		for(ComponentInstance virtualBus:virtualBusList)
		{// ensures that for any bus it is always taken in the same direction
			List<ComponentInstance> connectionBindingList = 
					PropertyUtils.getComponentInstanceList(virtualBus,
							"Actual_Connection_Binding");
			for(int i=2;i<connectionBindingList.size();i+=2)
			{
				//connection between i-1 and i+1, i is a bus
				ComponentInstance busIn = connectionBindingList.get(i-1);
				ComponentInstance ci = connectionBindingList.get(i);

				for(FeatureInstance fi: ci.getFeatureInstances()) { // for each connection of the component

					if(fi.getCategory()!=FeatureCategory.BUS_ACCESS) {
						String errMsg = "not a bus in even pos "+i;
						_LOGGER.error(errMsg);
						
						throw new MalformedInputException(errMsg, ci);
					}

					if(fi.getSrcConnectionInstances().size()>1 || fi.getDstConnectionInstances().size()>1
							|| fi.getSrcConnectionInstances().size() + fi.getDstConnectionInstances().size() > 1
							) { 
						String errMsg = "bus end is connected to two ports";
						_LOGGER.error(errMsg);
						throw new MalformedInputException(errMsg, ci);
					}
										
					
					if (fi.getSrcConnectionInstances().size() + fi.getDstConnectionInstances().size() == 0)
						continue;
					ConnectionInstanceEnd cie = fi.getSrcConnectionInstances().size() == 0 ? 
							fi.getDstConnectionInstances().get(0).getSource() : 
							fi.getSrcConnectionInstances().get(0).getDestination() ;

					if(cie.equals(busIn)) {
						List<Flow> featureFlowList = featureInstance2inFlowList.get(fi);
						
						if(featureFlowList == null)
						{
							featureFlowList = new ArrayList<Flow>();
							featureInstance2inFlowList.put(fi, featureFlowList);
						}
						featureFlowList.add(flowMap.get(virtualBus));
					} //else assert(false);
				}
			}
		}

		for(ComponentInstance nwkSwitch : nodesMap.keySet()) {
			Map<Integer, List<Flow>> m = new HashMap<Integer, List<Flow>>();
			for(FeatureInstance fi : nwkSwitch.getAllFeatureInstances()) // fi is the ports of the switch
			{
				Integer key = Integer.valueOf(nwkSwitch.getAllFeatureInstances().indexOf(fi));
				List<Flow> value =  featureInstance2inFlowList.get(fi);
				if(value == null) continue;
				assert(m.get(key) == null || m.get(key) == value);
				m.put(key, value);
			}
			nodesMap.get(nwkSwitch).setInputPort(m);
		}

		Network n = new Network(flows, nodes);
		n.init();

		_LOGGER.debug(n);

		Map<ComponentInstance, DetailLogger> results = new HashMap<ComponentInstance, DetailLogger>();
		Map<ComponentInstance, Double> deadlines = new HashMap<ComponentInstance, Double>();

		
		Algorithm2 algo = new Algorithm2(n);
		List<DetailLogger> resultList = algo.computeWorstCaseEndToEndResponse();
		int i=0;
		for(DetailLogger res: resultList)
		{
			Flow flow = n.getFlows().get(i);
			ComponentInstance ci = flow2CI.get(flow);
			results.put(ci, res );
			deadlines.put(ci, flow.getDeadline());
			i++;
			for(DetailLogger.Log l:res.cur.get(0).a) {
				if (l.n == null ) {
					_LOGGER.debug(" , , "+l.value);
				}
			}
			for(Node nn:flow.getPath().getNodes()) {
				for(DetailLogger.Log l:res.cur.get(0).a) {
					if (l.n==nn) {
						_LOGGER.debug( l.n.getId()+", "+(l.f==null ? "" : l.f.getId()) +", "+l.value);
					}
				}
			}


			_LOGGER.debug("Flow "+i+" : WCTT = "+res.cur.get(0).value);
		}
		
		return new TrajectoryAnalysisResult(results, deadlines, flowMap);
	}
}
