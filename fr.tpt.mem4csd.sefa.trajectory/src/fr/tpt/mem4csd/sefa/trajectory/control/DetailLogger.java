package fr.tpt.mem4csd.sefa.trajectory.control;

import java.util.ArrayList;
import java.util.List;

import fr.tpt.mem4csd.sefa.trajectory.model.Flow;
import fr.tpt.mem4csd.sefa.trajectory.model.Node;


/** Helper class to compute details of worst case response time
 * contains a list of LogAggregate 
 * **/
public class DetailLogger {

	/** A single log **/
	public class Log{
		Flow f;
		Node n;
		public Log(String s, double val){this.reason=s;this.value=val;};
		public Log(String s, double val, Flow f, Node n){this.reason=s;this.value=val;this.f=f;this.n=n;};
		public Log(String s, double val, Node n){this.reason=s;this.value=val;this.n=n;};
		String reason;
		double value;
	}
	
	/** A list of log **/
	public class LogAggregate{
		List<Log> a;
		double value;
		public LogAggregate(){this.a=new ArrayList<Log>();this.value=0.;}
		public void add(Log l) {this.a.add(l);this.value+=l.value;}
		public LogAggregate max(LogAggregate l){if(l.value>this.value)return l; else return this;}
		public void add(LogAggregate l){for(Log ll:l.a) {this.add(ll);};}
	}

	ArrayList<LogAggregate> cur;
	
	public DetailLogger() {this.cur=new ArrayList<LogAggregate>();}
	public void fork(){this.cur.add(new LogAggregate());}
	public void joinMax(){
		assert(this.cur.size()>1);
		LogAggregate m = this.cur.get(this.cur.size()-2).max(this.cur.get(this.cur.size()-1));
		this.cur.remove(this.cur.size()-1);
		this.cur.remove(this.cur.size()-1);	
		this.cur.add(m);
	}
	public void joinAdd(){
		assert(this.cur.size()>1);
		this.cur.get(this.cur.size()-2).add(this.cur.get(this.cur.size()-1));
		this.cur.remove(this.cur.size()-1);
	}
	public void log(String s, double val, Flow f) {
		this.cur.get(this.cur.size()-1).add(new Log(s,val,f,null));
	}
	public void log(String s, double val, Node n) {this.cur.get(this.cur.size()-1).add(new Log(s,val,n));}
	public void log(String s, double val, Flow f, Node n) {this.cur.get(this.cur.size()-1).add(new Log(s,val,f,n));}
	
}
