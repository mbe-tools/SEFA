package fr.tpt.mem4csd.sefa.trajectory.control;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import fr.tpt.mem4csd.sefa.trajectory.model.Flow;
import fr.tpt.mem4csd.sefa.trajectory.model.Network;
import fr.tpt.mem4csd.sefa.trajectory.model.Node;
import fr.tpt.mem4csd.sefa.trajectory.model.Path;
import fr.tpt.mem4csd.sefa.trajectory.tools.Utils;

import fr.tpt.mem4csd.sefa.trajectory.control.DetailLogger;

public class Algorithm2 {

	private Network net;
	public DetailLogger log;
	public List<DetailLogger> worstCasesResponseTime;

	public Algorithm2() {
		this.worstCasesResponseTime = new ArrayList<DetailLogger>();
	}

	public Algorithm2(Network net) {
		this.net = net;
		this.worstCasesResponseTime = new ArrayList<DetailLogger>();
	}

	public Network getNet() {
		return net;
	}

	public void setNet(Network net) {
		this.net = net;
	}

	/*******************************************/

	public Node firstNodeVisitedByJonI(Flow j, Flow i) {
		Path Pi = i.getPath();
		Path Pj = j.getPath();
		Node first = null;
		for (Node n : Pj.getNodes()) {
			if (Pi.getNodes().contains(n)) {
				first = n;
				return first;
			}
		}
		return first;
	}

	/**********************************************/

	public Node lastNodeVisitedByJonI(Flow j, Flow i) {
		Path Pi = i.getPath();
		Path Pj = j.getPath();
		Node last = null;
		for (Node n : Pj.getNodes()) {
			if (Pi.getNodes().contains(n)) {
				last = n;
			}
		}
		return last;
	}

	/********************/

	/* Restrict path study to flow i */
	public Node firstNodeVisitedByJonIRestrictedToH(Flow j, Flow i, Node h) {
		Node first = null;
		Path p = i.getPath().pathRestrictedToH(h);
		List<Node> iSubNodesList = p.getNodes();
		List<Node> jNodesList = j.getPath().getNodes();

		for (Node n : jNodesList) {
			if (iSubNodesList.contains(n)) {
				first = n;
				return first;
			}
		}
		return first;
	}

	/*****************************/

	public Node lastNodeVisitedByJonIRestrictedToH(Flow j, Flow i, Node h) {
		Node last = null;
		Path p = i.getPath().pathRestrictedToH(h);
		/*
		 * Asma : ce test est utile dans le cas o� on cherche lastIJH et non pas
		 * lastJIH cas pour ce dernier P n'est jamais null puisque h appartient
		 * au flot i
		 */
		if (p != null) {
			List<Node> iSubNodesList = p.getNodes();
			List<Node> jNodesList = j.getPath().getNodes();

			for (Node n : jNodesList) {
				if (iSubNodesList.contains(n)) {
					last = n;
				}
			}
		}
		return last;
	}

	/******************************/
	/** Sums Lmin for every edge, + capacity for every device for this flow, up to *entry* in h **/
	public double minTimeTakenFromSourceToH(Flow f, Node h) {

		Path p = f.getPath().pathRestrictedToH(h);
		double res = f.getLmin() * (p.getNodes().size() - 1);

		/* je ne comprends pas pourquoi la boucle commence par 1 au lieu de 0 ? */

		for (int i = 0; i < p.getNodes().size() - 1; i++) {
			res += p.getNodes().get(i).getCapacity().get(f);
		}
		return res;
	}

	/******************************/
	/** Sums Lmax for every edge, + capacity for every device for this flow, up to *entry* in h **/
	public double maxTimeTakenFromSourceToH(Flow f, Node h) {

		Path p = f.getPath().pathRestrictedToH(h);
		double res = f.getLmax() * (p.getNodes().size() - 1);
		/* je ne comprends pas pourquoi la boucle commence par 1 au lieu de 0 ? */
		for (int i = 0; i < p.getNodes().size() - 1; i++) {
			res += p.getNodes().get(i).getCapacity().get(f);
		}
		return res;
	}

	/***********************************/

	/**
	 * In research paper : slowest node visited by flow i on path j compared
	 * capacity is capacity of flow j If there exists several nodes with lowest
	 * capacity we select the last on on the path
	 * @return the node in intersectNodesINodesJ for which the flow j has the larger execution time (capacity)
	 */
	public Node slowestNodeVisitedByJonI(Flow j, Flow i) {
		Node res = null;
		List<Node> nodesI = i.getPath().getNodes();
		List<Node> nodesJ = j.getPath().getNodes();
		List<Node> intersectNodesINodesJ = Utils.getIntersectionList(nodesJ,
				nodesI); //ordered by J

		for (Node x : intersectNodesINodesJ) {
			if (intersectNodesINodesJ.indexOf(x) == 0) {
				res = x;
			} else {
				if (x.getCapacity().get(j) >= res.getCapacity().get(j)) {
					res = x;
				}
			}
		}
		return res;
	}

	/*****************************/

	public Node slowestNodeVisitedByIonHisPathRestrictedToH(Flow i, Node h) {
		Path p = i.getPath().pathRestrictedToH(h);
		Node res = p.getNodes().get(0);
		for (Node node : p.getNodes()) {
			if (node.getCapacity().get(i) >= res.getCapacity().get(i)) {
				res = node;
			}
		}
		return res;
	}

	/*****************************/

	public Node slowestNodeVisitedByJonIRestrictedToH(Flow j, Flow i, Node h) {
		Node res = null;
		List<Node> iSubNodesList = i.getPath().pathRestrictedToH(h).getNodes();

		for (Node nj : j.getPath().getNodes()) {
			if (iSubNodesList.contains(nj)) {
				if (res == null) {
					res = nj;
				} else {
					if (nj.getCapacity().get(j) >= res.getCapacity().get(j)) {
						res = nj;
					}
				}
			}
		}
		return res;
	}

	/***************************************/

	double computeM(Flow i, Node h) {

		double m = 0;
		if (i.getPath().getNodes().get(0) != h) {

			Node predHinI = nodePreceedingHinFlowI(i, h);
			Path pathIrestrictedToPredH = i.getPath().pathRestrictedToH(
					predHinI);

			for (Node hprime : pathIrestrictedToPredH.getNodes()) {

				double min = hprime.getCapacity().get(i);

				for (Flow j : Utils.getUnionList(i.getHigherPriorityFlows(),
						i.getSamePriorityFlows(), i)) {
					if(hprime.getCapacity().get(j)==null)
						continue;
					if (firstNodeVisitedByJonI(j, i) == firstNodeVisitedByJonI(
							i, j) && firstNodeVisitedByJonI(j, i) != null) {
						min = Math.min(min, hprime.getCapacity().get(j));
					}
				}
				m = m + min + i.getLmin();
			}
		}
		return m;
	}

	/******************************/

	Node nodePreceedingHinFlowI(Flow i, Node h) {

		Node predH = null;
		for (int n = 0; n < i.getPath().getNodes().size(); n++) {
			if (h == i.getPath().getNodes().get(n)) {
				predH = i.getPath().getNodes().get(n - 1);
			}
		}
		return predH;
	}

	/********************************/

	double computeARestrictedToH(Flow i, Flow j, Node H) {
		double result = j.getJitter();

		Node firstIJ = firstNodeVisitedByJonIRestrictedToH(i, j, H);
		/*
		 * Asma : ce test peut etre elimine puisque computeARestrictedToH est
		 * appelee uniquement lorsque lastIJ != null et si lastIJ != null alors
		 * firstIJ ne peux pas etre null
		 */
		if (firstIJ != null) {
			double m = computeM(i, firstIJ);
			double smax = maxTimeTakenFromSourceToH(j, firstIJ);
			result = result + smax - m;
		}
		return result;
	}


	/********************************/

	/** Compute the delay due to non-preemption **/

	double computeDelta(Flow i, Node hrestriction) {
		double delta = 0;

		// ///////////////////
		double max = 0;
		Node firsti = i.getPath().getNodes().get(0);
		for (Flow j : i.getLowerPriorityFlows()) {
			if (firstNodeVisitedByJonI(j, i) == firsti) {
				double cap = firsti.getCapacity().get(j);
				max = Math.max(max, cap);
			}
		}
		if (max - 1 > 0) {
			delta += max - 1;
		}
		// ///////////////////

		Path p = i.getPath().pathRestrictedToH(hrestriction);

		for (Node h : p.getNodes()) {
			if (h != firsti) {
				double max1 = 0, max2 = 0, max3 = 0;
				for (Flow j : i.getLowerPriorityFlows()) {

					if (firstNodeVisitedByJonI(j, i) == h) {
						double cap = h.getCapacity().get(j);
						max1 = Math.max(max1, cap);
					}

					if (firstNodeVisitedByJonI(j, i) != h
							&& firstNodeVisitedByJonI(j, i) != null) {
						if (firstNodeVisitedByJonI(j, i) != firstNodeVisitedByJonI(
								i, j)) {
							double cap = h.getCapacity().get(j);
							max2 = Math.max(max2, cap);
						}
					}

					if (firstNodeVisitedByJonI(j, i) != h
							&& firstNodeVisitedByJonI(j, i) != null) {
						if (firstNodeVisitedByJonI(j, i) == firstNodeVisitedByJonI(
								i, j)) {
							Double cap = h.getCapacity().get(j);
							if (cap != null)
								max3 = Math.max(max3, cap);
						}
					}

				}
				if (max1 - 1 > 0) {
					delta += max1 - 1;
				}
				if (max2 - 1 > 0) {
					delta += max2 - 1;
				}

				if (i.getLowerPriorityFlows().size() != 0) {
					
					double lMax = i.getLowerPriorityFlows().get(0).getLmax();
					double lMin = i.getLowerPriorityFlows().get(0).getLmin();
					
					for(Flow f: i.getLowerPriorityFlows())
						if(f.getLmax()>lMax)
							lMax = f.getLmax();
					
					for(Flow f: i.getLowerPriorityFlows())
						if(f.getLmin()<lMin)
							lMin = f.getLmin();
					
					// TODO: check if not too pessimistic
					double val = max3
							- nodePreceedingHinFlowI(i, h).getCapacity().get(i)
							+ lMax - lMin;
					if (val > 0) {
						delta += val;
					}
				}
			}
		}
		return delta;
	}

	/*****************************************/

	void computeW(Flow i, int t) {
		HashMap<Node, Integer> w = new HashMap<Node, Integer>();
		int w1 = 0;
		int w2 = 0;
		int max = 0;
		this.log.fork();
		for (Node h : i.getPath().getNodes()) {
			this.log.fork();
			w1 = subfunction_computeW_initialize_sequence(i, t, h);
			this.log.fork();
			max = w1;
			w2 = subfunction_computeW_nextof_sequence(i, t, h, w1, w);
			this.log.joinMax();
			while (w1 != w2) {
				if (w2 > max)
					max = w2;
				w1 = w2;
				this.log.fork();
				w2 = subfunction_computeW_nextof_sequence(i, t, h, w1, w);
				this.log.joinMax();

			}
			w.put(h, max);
			this.log.joinMax();
		}
		this.log.joinAdd();
	}

	/************************************/

	int subfunction_computeW_initialize_sequence(Flow i, int t, Node h) {
		this.log.fork();
		int w0 = 0;

		for (Flow j : Utils.getUnionList(i.getHigherPriorityFlows(),
				i.getSamePriorityFlows())) {
			if (slowestNodeVisitedByJonIRestrictedToH(j, i, h) != null) {
				Node slow = slowestNodeVisitedByJonIRestrictedToH(j, i, h);
				w0 += slow.getCapacity().get(j);
				this.log.log("slowed by flow "+j.getId()+" on switch "+slow.getId(), slow.getCapacity().get(j),j,slow);
			}
		}

		Node slow = slowestNodeVisitedByIonHisPathRestrictedToH(i, h);

//		Commented: seems to be counted twice
//		double x = (1 + (int) Math.floor(((double) (t + i.getJitter()))
//				/ (double) (i.getPeriod())))
//				* slow.getCapacity().get(i);
//		
//		w0+=x;
//		this.log.log("slowest node on flow path "+slow.getId(), x,i,slow);

		for (Node k : i.getPath().pathRestrictedToH(h).getNodes()) {
			if (k != slow) {
				double max = 0; // for this node, biggest capacity for all flows that cross flow i and are at least as important (and have a common first point)
				Flow jmax = new Flow();
				for (Flow j : Utils.getUnionList(i.getHigherPriorityFlows(),
						i.getSamePriorityFlows(), i)) {
					if(k.getCapacity().get(j)==null)
						continue;
					if (firstNodeVisitedByJonI(j, i) == firstNodeVisitedByJonI(
							i, j) && firstNodeVisitedByJonI(j, i) != null) {
						if (k.getCapacity().get(j) > max) {
							max = k.getCapacity().get(j);
							jmax=j;
						}
						else if (k.getCapacity().get(j) == max && j==i)
						{
							jmax=i;
						}
					}
				}
				w0 += max;
				if(max > 0)
					if(jmax != i)
						this.log.log("slowed by flow "+jmax.getId()+" on switch "+k.getId(), max,jmax,k);
					else
						this.log.log("flow traverses on switch "+k.getId(), max,i,k);
			}
		}
		
		double suppl = 0;

//		double lastCapacity = h.getCapacity().get(i);
		double nonPreemptionDelay = computeDelta(i, h);
		double deltaOpt = computeDeltaOpt(i, t, h, 0);

//		suppl -= lastCapacity;
		suppl += nonPreemptionDelay;
		suppl -= deltaOpt;

		double busTraversal = ((i.getPath().pathRestrictedToH(h).getNodes().size()) - 1)
				* i.getLmax();
		suppl += busTraversal;
		
		w0+=suppl;
		
		this.log.log("Traversal of buses (" + i.getLmax() +"*"+
						((i.getPath().pathRestrictedToH(h).getNodes().size()) - 1)+")", busTraversal, i);
	
		
		this.log.log("Delay due to non-preemption of lower priority flows", nonPreemptionDelay, i);
//		double opt = lastCapacity+deltaOpt;
		double opt = deltaOpt;
		this.log.log("Reduce pessimism: frames are serialized", -1.0*opt, i);	
		//W456 in equations
		this.log.joinAdd();
		return w0;
	}

	/***********************************/

	int subfunction_computeW_nextof_sequence(Flow i, int t, Node h, int w1,
			HashMap<Node, Integer> w) {
		int w2 = 0;
		this.log.fork();
		double deltaOpt = 0;
		// // compute the first term of the W equation
		for (Flow j : i.getHigherPriorityFlows()) {
			Node slow = slowestNodeVisitedByJonIRestrictedToH(j, i, h);
			Node lastjih = lastNodeVisitedByJonIRestrictedToH(j, i, h);
			int val = 0;
			if (slow != null && lastjih != null) {
				if (lastjih == h || w.get(lastjih) == null) {
					val = 1 + (int) Math
							.floor((double) (w1
									- minTimeTakenFromSourceToH(j, h) + computeARestrictedToH(
										i, j, h)) / (double) (j.getPeriod()));

				} else {
					val = 1 + (int) Math
							.floor((double) (w.get(lastjih)
									- minTimeTakenFromSourceToH(j, lastjih) + computeARestrictedToH(
										i, j, h)) / (double) (j.getPeriod()));

				}
				if (w.get(h) == null) {
					deltaOpt = computeDeltaOpt(i, t, h, w1);
				} else {
					deltaOpt = computeDeltaOpt(i, t, h, w.get(h));
				}
				if (val < 0) {
					val = 0;
				}
				this.log.log("slowed by higher priority flow "+j.getId()+" on switch "+slow.getId(), val*slow.getCapacity().get(j), j, slow);
				w2 += val * slow.getCapacity().get(j);
			}
		}
		// // compute the second term of the W equation

		List<Flow> samePriorityUnionI = Utils.getUnionList(new ArrayList<Flow>(),
				i.getSamePriorityFlows(), i);
		for (Flow j : samePriorityUnionI) {

			Node slow = slowestNodeVisitedByJonIRestrictedToH(j, i, h);
			Node firstjih = firstNodeVisitedByJonIRestrictedToH(j, i, h);

			int val = 0;
			if (slow != null && firstjih != null) {

				val = 1 + (int) Math
						.floor((double) (t
								+ maxTimeTakenFromSourceToH(i, firstjih)
								- minTimeTakenFromSourceToH(j, firstjih) + computeARestrictedToH(
									i, j, h)) / (double) (j.getPeriod()));

				if (val < 0) {
					val = 0;
				}
				w2 += val * slow.getCapacity().get(j);
				if(i!=j)
					this.log.log("slowed by same priority flow "+j.getId()+" on switch "+slow.getId(), val*slow.getCapacity().get(j),j,slow);
				else
					this.log.log("current flow ("+j.getId()+") traverses switch "+slow.getId(), val*slow.getCapacity().get(j),j,slow);
			}
		}

		// // compute the third term of the W equation

		Node slow = slowestNodeVisitedByIonHisPathRestrictedToH(i, h);

		for (Node k : i.getPath().pathRestrictedToH(h).getNodes()) {
			if (k != slow) {

				double max = 0;
				Flow jmax = new Flow();
				for (Flow j : Utils.getUnionList(i.getHigherPriorityFlows(),
						i.getSamePriorityFlows(), i)) {
					if(k.getCapacity().get(j)==null)
						continue;
					if (firstNodeVisitedByJonIRestrictedToH(j, i, h) == firstNodeVisitedByJonIRestrictedToH(
							i, j, h)
							&& firstNodeVisitedByJonIRestrictedToH(j, i, h) != null) {

						double cap = k.getCapacity().get(j);
						if (cap > max) {
							max = cap;
							jmax=j;
						}

					}
				}
				w2 += max;
				if(max > 0)
				{
					if(jmax!=i)
						this.log.log("slowed by "+jmax.getId()+" on switch "+k.getId(), max,jmax,k);
					else
						this.log.log("current flow ("+jmax.getId()+") traverses switch "+k.getId(), max,jmax,k);
				}
			}

		}

		// // compute the fourth, fifth and sixth terms of the W equation
		
		double suppl = 0;

		double lastCapacity = h.getCapacity().get(i);
		double nonPreemptionDelay = computeDelta(i, h);
		suppl -= lastCapacity;
		suppl += nonPreemptionDelay;
		suppl -= deltaOpt;

		double busTraversal = ((i.getPath().pathRestrictedToH(h).getNodes().size()) - 1)
				* i.getLmax();
		suppl += busTraversal;
		
		w2+=suppl;
//		this.log.log("W_next", w2);
		
		this.log.log("Traversal of buses (" + i.getLmax() +"*"+
						((i.getPath().pathRestrictedToH(h).getNodes().size()) - 1)+")", busTraversal, i);
	
		
		this.log.log("Delay due to non-preemption ", nonPreemptionDelay-(lastCapacity+deltaOpt), i);		
		
		
		/*
		 * This functions computes the lower bound of the delta term to optimise
		 * the basic computation of the trajectory approach. Here delta (deltaOpt)
		 * is different from delta representing the delay due to the non-preemtion
		 * */
		
		this.log.joinAdd();
		return w2;
	}

	/*****************************/

	public List<DetailLogger> computeWorstCaseEndToEndResponse() {
		List<Flow> flows = net.getFlows();
		int t;
		for (Flow i : flows) {
			this.log = new DetailLogger();
			this.log.fork();
			for (t = (int) (-(i.getJitter())); t < -(i.getJitter()) + 1; t++) {
				Path path = i.getPath();
				List<Node> nodes = path.getNodes();
				Node last_i = nodes.get(nodes.size() - 1);
				this.log.fork();

				computeW(i, t); 
				this.log.log("Flow jitter", last_i.getCapacity().get(i)- t, i);
				this.log.joinMax();
			}
			worstCasesResponseTime.add(this.log);
			assert(this.log.cur.size()==1);
			i.setWcrt(this.log.cur.get(0).value);
		}
		return worstCasesResponseTime;
	}

	/**
	 * This functions computes the lower bound of the delta term to optimise the
	 * basic computation of the trajectory approach.
	 * 
	 * Here delta (deltaOpt) is different from delta representing the delay due
	 * to the non-preemtion
	 * 
	 * @param i
	 * @param t
	 * @param h
	 * @param wITH
	 * @return
	 */
	public double computeDeltaOpt(Flow i, int t, Node h, int wITH) {
		double deltaOpt = 0;
		double max1 = 0;
		// compute upper bound of (a_f(h))
		for (int inport : h.getInputPort().keySet()) {
			// /// Compute the first and second term of the equation computing
			// deltaopt
			double sum1 = 0, max2 = 0;
			for (Flow j : Utils.getUnionList(new ArrayList<Flow>(),
					i.getSamePriorityFlows(), i)) {
				if (h.getInputPort().get(inport).contains(j)) {
					double aIJ = computeARestrictedToH(i, j, h);
					if ((1 + (int) (Math.floor((double) (t + aIJ)
							/ (double) j.getPeriod()))) > 0) {
						sum1 += (1 + (int) (Math.floor((t + aIJ)
								/ j.getPeriod()))
								* h.getCapacity().get(j));
						max2 = Math.max(max2, h.getCapacity().get(j));
					}
				}
			}
			sum1 = sum1 - max2;
			max1 = Math.max(max1, sum1);
		}
		deltaOpt += max1;

		// compute the lower bound of (a_p(h-1))
		// ici il faut tout d'abord r�cup�rer le port d'entr�e du flot i
		int portI = 0; // je rappelle que le num�ro de port d'entr�e pour un
						// noeud commence par 1

		for (int inport : h.getInputPort().keySet()) {
			if ((h.getInputPort().get(inport)).contains(i))
				portI = inport;
		}

		// //// Compute the third term of the equation computing delta opt
		int sum2 = 0;
		for (Flow j : Utils.getUnionList(new ArrayList<Flow>(),
				i.getSamePriorityFlows(), i)) {
			if(h.getInputPort().isEmpty())
				continue;
			if (h.getInputPort().get(portI) == null)
				continue;
			if (h.getInputPort().get(portI).contains(j)) {
				double aIJ = computeARestrictedToH(i, j, h);
				if ((1 + (int) (Math.floor((double) (t + aIJ)
						/ (double) j.getPeriod()))) > 0) {
					sum2 += (1 + (int) (Math.floor((double) (t + aIJ)
							/ (double) j.getPeriod())))
							* h.getCapacity().get(j);
				}
			}
		}

		deltaOpt -= sum2;

		// //// Compute the fourth term of the equation computing delta opt

		double min1 = h.getCapacity().get(i);
		for (Flow j : Utils.getUnionList(new ArrayList<Flow>(),
				i.getSamePriorityFlows(), i)) {
			if(h.getInputPort().isEmpty())
				continue;
			if(h.getInputPort().get(portI)==null)
				continue;
			if (h.getInputPort().get(portI).contains(j)) {
				min1 = Math.min(min1, h.getCapacity().get(j));
			}
		}
		deltaOpt += min1;

		deltaOpt = Math.max(deltaOpt, 0);
		return deltaOpt;
	}

	/********************************/
}
