package fr.tpt.mem4csd.sefa.trajectory.tools;



import java.util.ArrayList;
import java.util.List;

import fr.tpt.mem4csd.sefa.trajectory.model.Flow;
import fr.tpt.mem4csd.sefa.trajectory.model.Node;

public class Utils {

	// returns the list of common nodes between the flow i and the flow j
	public static List<Node> getIntersectionList(List<Node> nodesJ,
			List<Node> nodesI) {
		// TODO Auto-generated method stub
		List<Node> intersectNodesINodesJ = new ArrayList<Node>();
		for (Node j : nodesJ) {
			if (nodesI.contains(j)) {
				intersectNodesINodesJ.add(j);
			}
		}
		return intersectNodesINodesJ;
	}

	// returns the list of flows with higher or equal priority than the flow i
	// including the flow i
	public static List<Flow> getUnionList(List<Flow> higherPriorityFlows,
			List<Flow> samePriorityFlows, Flow i) {
		// TODO Auto-generated method stub
		List<Flow> res = new ArrayList<Flow>();
		for (Flow f : higherPriorityFlows) {
			res.add(f);
		}
		for (Flow f : samePriorityFlows) {
			res.add(f);
		}
		res.add(i);
		return res;
	}
	
	public static List<Flow> getUnionList(List<Flow> higherPriorityFlows,
			List<Flow> samePriorityFlows) {
		// TODO Auto-generated method stub
		List<Flow> res = new ArrayList<Flow>();
		for (Flow f : higherPriorityFlows) {
			res.add(f);
		}
		for (Flow f : samePriorityFlows) {
			res.add(f);
		}
		return res;
	}
}
