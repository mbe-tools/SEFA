package fr.tpt.mem4csd.sefa.examples;

import fr.tpt.mem4csd.utils.eclipse.ui.AbstractExampleWizard;

/**
 * Create the example projects.
 */
public class SefaExamplesWizard extends AbstractExampleWizard {


	@Override
	protected String[] getProjectNames() {
		return new String[]{"reference_example", "industrial_example" };
	}

	@Override
	protected String getPluginId() {
		return "fr.tpt.mem4csd.sefa.examples";
	}
	@Override
	protected String getExamplesSourceDir() {
		return "examples_src";
	}
}
